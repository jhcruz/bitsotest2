process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/**
 * Principal function
 * @returns {undefined}
 */
function main() {
    var testCases = readLine();
    getTestCases(testCases);
}
/**
 * Getting the archipielagos
 * @param {type} islands
 * @returns {Array}
 */
function getTotalArchipielago(islands) {
    var archipielagos = [];
    for (var i = 0; i < islands.length; i++) {
        var endpointIsland = islands[i];
        var neighborDistances = [];
        for (var j = 0; j < islands.length; j++) {
            if (j === i) {
                continue;
            }
            var neighbor = islands[j];
            var distanceEndPoint = islandDistance(
                    endpointIsland.x, endpointIsland.y,
                    neighbor.x, neighbor.y);
            neighborDistances.push({neighbor, distanceEndPoint});
        }
        archipielagos = evaluateSegments(neighborDistances, endpointIsland, archipielagos);
    }
    console.log(archipielagos.length);
}

/*
 * Function to evaluate the segments
 */
function evaluateSegments(neighborDistances, endpointIsland, archipielagos) {
    for (var j = 0; j < neighborDistances.length; j++) {
        var neighbor1 = neighborDistances[j];
        for (var k = 0; k < neighborDistances.length; k++) {
            if (k === j) {
                continue;
            }
            var neighbor2 = neighborDistances[k];
            if (neighbor1.distanceEndPoint === neighbor2.distanceEndPoint) {
                var archipielago1 = [
                    endpointIsland,
                    neighbor1.neighbor,
                    neighbor2.neighbor
                ];
                // if not exis the archipielago is saved
                if(existArchipielago(archipielagos,archipielago1)){
                    archipielagos.push(archipielago1);
                   // console.log('archipielago:' + JSON.stringify(archipielagos) + '\n');
                } 
            }
        }
    }
    return archipielagos;
}

/**
 * Evaluate if exist archipielagos
 * @param {type} archipielagos
 * @param {type} archipielago
 * @returns {undefined}
 */
function existArchipielago(archipielagos,archipielago){
    console.log(archipielago);
    return  archipielagos.filter(e=>isEquivalent(e,archipielago)).length === 0;
}

/**
 * Object to compare
 * @param {type} a
 * @param {type} b
 * @returns {Boolean}
 */
function isEquivalent(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}
/**
 * Calculating the distance between islands
 * @param {type} x1
 * @param {type} y1
 * @param {type} x2
 * @param {type} y2
 * @returns {Number}
 */
function islandDistance(x1, y1, x2, y2) {
    if (!x2)
        x2 = 0;
    if (!y2)
        y2 = 0;
    return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

/**
 * separsting the test cases
 * @param {type} testCases
 * @returns {Array|getTestCases.islandsTestCase}
 */
function getTestCases(testCases) {
    for (var testCaseIterator = 1; testCaseIterator <= testCases; testCaseIterator++) {
        var islandsTestCase = [];
        var numberOfIslands = readLine();
        for (var i = 0; i < numberOfIslands; i++) {
            var islandPositions = readLine();

            islandsTestCase.push({
                x: parseInt(islandPositions.split(' ')[0]),
                y: parseInt(islandPositions.split(' ')[1])
            });
        }
        getTotalArchipielago(islandsTestCase);
    }
}


